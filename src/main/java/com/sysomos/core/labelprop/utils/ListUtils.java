package com.sysomos.core.labelprop.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

public class ListUtils {

	public static List<Long> newList(long userId1, long userId2) {
		List<Long> ids = new ArrayList<Long>();
		ids.add(userId1);
		ids.add(userId2);
		return ids;
	}

	public static String join(Iterable<?> l, String glue) {
		if (l == null)
			return null;
		StringBuffer sb = new StringBuffer();
		boolean first = true;
		for (Object o : l) {
			if (!first) {
				sb.append(glue);
			}
			sb.append(o.toString());
			first = false;
		}
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] toArray(List<T> list) {
		if (CollectionUtils.isEmpty(list))
			return null;
		Object[] ret = new Object[list.size()];
		list.toArray(ret);
		return (T[]) ret;
	}
}
