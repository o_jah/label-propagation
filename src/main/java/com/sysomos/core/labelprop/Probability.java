package com.sysomos.core.labelprop;

import java.util.List;

import com.sysomos.core.labelprop.utils.ListUtils;

public class Probability {

	public List<Double> probScores;

	public Probability(List<Double> probScores) {
		this.probScores = probScores;
	}

	public String toString() {
		return ListUtils.join(probScores, ", ");
	}
}
