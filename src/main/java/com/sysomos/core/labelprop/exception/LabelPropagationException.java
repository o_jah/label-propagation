package com.sysomos.core.labelprop.exception;

public class LabelPropagationException extends Exception {

	public LabelPropagationException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
