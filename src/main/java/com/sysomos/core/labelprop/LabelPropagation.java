package com.sysomos.core.labelprop;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ejml.simple.SimpleMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.core.labelprop.exception.LabelPropagationException;

public class LabelPropagation {
	private static Logger LOG = LoggerFactory.getLogger(LabelPropagation.class);

	private SimpleMatrix transitionMatrix;
	private SimpleMatrix labTransMatrix;
	private SimpleMatrix unLabTransMatrix;
	private SimpleMatrix classProbMatrix;

	/**
	 * Constructor
	 * 
	 * @param numLabInstances
	 *            Number of label instances (instances with valid class label)
	 * @param classProbs
	 *            Class probability distribution
	 * @param transWeights
	 *            Transition weights obtained from the similarity graph. Ensure
	 *            that the first numLabInstances rows and columns of the matrix
	 *            correspond to the labeled instances.
	 * @throws LabelPropagationException
	 *             Thrown when the transition weight is null, invalid, or when
	 *             the class probabilities matrix is null or invalid.
	 */
	public LabelPropagation(int numLabInstances, final double[][] classProbs, final double[][] transWeights)
			throws LabelPropagationException {
		initialize(numLabInstances, classProbs, transWeights);
	}

	private void initialize(int numLabInstances, final double[][] classProbs, final double[][] transWeights)
			throws LabelPropagationException {

		if (transWeights == null || transWeights.length <= numLabInstances || classProbs == null
				|| classProbs.length != numLabInstances) {
			throw new LabelPropagationException("Encountered error when computing weights: transWeights: "
					+ transWeights.length + "numLabInstances : " + numLabInstances);
		}

		transitionMatrix = new SimpleMatrix(rowColNormalize(transWeights));
		unLabTransMatrix = transitionMatrix.extractMatrix(numLabInstances, transWeights.length, numLabInstances,
				transWeights.length);
		labTransMatrix = transitionMatrix.extractMatrix(numLabInstances, transWeights.length, 0, numLabInstances);
		classProbMatrix = new SimpleMatrix(classProbs);
	}

	private double[][] rowNormalize(double[][] weights) throws LabelPropagationException {
		if (weights == null) {
			throw new LabelPropagationException("Failed to normalize weights. Input array is null.");
		}
		for (int i = 0; i < weights.length; i++) {
			double sum = aggregate(weights[i]);
			if (sum == 0) {
				continue;
			}
			for (int j = 0; j < weights[i].length; j++) {
				weights[i][j] /= sum;
			}
		}
		return weights;
	}

	private double[][] colNormalize(double[][] weights) throws LabelPropagationException {
		if (weights == null) {
			throw new LabelPropagationException("Failed to normalize weights. Input array is null.");
		}
		int currRow = 0;
		for (int j = 0; j < weights[currRow].length; j++) {
			double sum = 0;
			for (int i = 0; i < weights.length; i++) {
				sum += weights[i][j];
			}
			if (sum == 0) {
				continue;
			}
			for (int i = 0; i < weights.length; i++) {
				weights[i][j] /= sum;
			}
		}

		return weights;
	}

	private double[][] rowColNormalize(double[][] weights) throws LabelPropagationException {
		return rowNormalize(colNormalize(weights));
	}

	private double aggregate(final double[] weights) throws LabelPropagationException {
		if (weights == null) {
			throw new LabelPropagationException("Failed to aggregate weights. Input array is null.");
		}
		double sum = 0;
		for (int i = 0; i < weights.length; i++) {
			sum += weights[i];
		}
		return sum;
	}

	SimpleMatrix fixpoint;

	/**
	 * This method is called to run the algorithm.
	 */
	public Map<Integer, ClassLabel> label() throws LabelPropagationException {
		LOG.info("LPSentimentEnhancer.computeFixpoint:startTime " + new Date().toString());

		fixpoint = computeFixpoint();

		if (fixpoint != null) {
			LOG.info("FP : [ rows: " + fixpoint.numRows() + ", cols: " + fixpoint.numCols() + " ] ");
		}

		Map<Integer, ClassLabel> inferredLabels = new HashMap<Integer, ClassLabel>();
		for (int i = 0; i < unLabTransMatrix.numRows(); i++) {
			int maxProbIndex = 0;
			double maxProb = fixpoint.get(i, 0);
			List<Double> probScores = new ArrayList<Double>();
			for (int j = 0; j < fixpoint.numCols(); j++) {
				probScores.add(fixpoint.get(i, j));
				if (fixpoint.get(i, j) >= maxProb) {
					maxProb = fixpoint.get(i, j);
					maxProbIndex = j;
				}
			}
			inferredLabels.put(i, new ClassLabel(maxProbIndex, new Probability(probScores)));
		}
		LOG.info("LabelPropagation.computeFixpoint:endTime " + new Date().toString());

		return inferredLabels;
	}

	private SimpleMatrix computeFixpoint() throws LabelPropagationException {
		unLabTransMatrix = SimpleMatrix.identity(unLabTransMatrix.numRows()).minus(unLabTransMatrix);

		LOG.info("Inverting Matrix");
		unLabTransMatrix = unLabTransMatrix.invert();
		LOG.info("Done inverting");

		if ((unLabTransMatrix.numCols() != labTransMatrix.numRows())
				|| (labTransMatrix.numCols() != classProbMatrix.numRows())) {
			LOG.info("UL: [ rows: " + unLabTransMatrix.numRows() + ", cols: " + unLabTransMatrix.numCols() + " ]");
			LOG.info("LM: [ rows: " + labTransMatrix.numRows() + ", cols: " + labTransMatrix.numCols() + " ]");
			LOG.info("YL: [ rows: " + classProbMatrix.numRows() + ", cols: " + classProbMatrix.numCols() + " ]");
			throw new LabelPropagationException("Failed to multiply matrices. Check Matrices dimensions.");
		}
		return unLabTransMatrix.mult(labTransMatrix.mult(classProbMatrix));
	}
}
