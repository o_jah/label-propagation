package com.sysomos.core.labelprop;

public class ClassLabel {

	private int classLabel;
	private Probability probability;

	public ClassLabel(int classLabel, Probability probability) {
		this.classLabel = classLabel;
		this.probability = probability;
	}

	public String toString() {
		return "Class Label: " + classLabel + "; Prob. Distrib: " + probability.toString();
	}
}
