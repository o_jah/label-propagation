package com.sysomos.core.labelprop;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.sysomos.core.labelprop.exception.LabelPropagationException;

public class LabelPropagationTest {

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	private LabelPropagation instance;
	private static double[][] classProbs;
	private static double[][] transWeights;

	static {
		transWeights = new double[][] { 
			{ 0.70, 0.85, 0.21, 0.42, 0.72 }, 
			{ 0.05, 0.40, 0.37, 0.48, 0.27 },
			{ 0.84, 0.73, 0.12, 0.60, 0.33 }, 
			{ 0.32, 0.49, 0.27, 0.33, 0.47 }, 
			{ 0.70, 0.40, 0.12, 0.72, 0.48 } 
		};

		classProbs = new double[][] { 
			{ 0.17, 0.13, 0.28, 0.02, 0.30 }, 
			{ 0.28, 0.14, 0.26, 0.32, 0.10 } 
		}; 
		
	}

	@Before
	public void setUp() {
		try {
			instance = new LabelPropagation(2, classProbs, transWeights);
		} catch (LabelPropagationException e) {
		}
	}
	
	@Test
	public void labelTest() {
		try {
			System.out.println(instance.label());
		} catch (LabelPropagationException e) {
		}
	}

}
